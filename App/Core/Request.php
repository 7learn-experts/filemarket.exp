<?php

namespace App\Core;

use App\Utilities\Input;

class Request
{

    public $method;
    public $uri;
    public $ip;
    public $agent;
    public $referer;
    public $params;     // $_REQUEST
    public $files;

    public function __construct()
    {
        if (SANITIZE_ALL_DATA) {
            $this->params = Input::clean($_REQUEST);
        } else {
            $this->params = $_REQUEST;
        }

        // $keys = array_keys($this->params);
        // foreach ($keys as $key) {
        //     $this->{$key} = $this->param($key);
        // }

        $this->files = $_FILES;
        $this->method = strtolower($_SERVER['REQUEST_METHOD']);
        $this->uri = $_SERVER['REQUEST_URI'];
        $this->ip = $_SERVER['REMOTE_ADDR'];
        $this->agent = $_SERVER['HTTP_USER_AGENT'];
        $this->referer = $_SERVER['HTTP_REFERER'] ?? '';
    }

    public function key_exists($key)
    {
        return in_array($key, array_keys($this->params));
    }

    public function isAjax()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            return true;
        }
        return false;
    }

    public function is_in($methods_arr)
    {
        return in_array($this->method, $methods_arr);
    }

    public function param($key)
    {
        return $this->params[$key];
    }

    public function __set($key, $value)
    {
        // if (in_array($key, ['loghman', 'mehrzad'])) {
        //     echo "Persmision Denied!";
        // }
    }
    public static function redirect($address)
    {
        if (filter_var($address, FILTER_VALIDATE_URL)) {
            header("Location: " . $address);
            die();
        }
        header("Location: " . site_url($address));
        die();
    }

    public function __get($key)
    {
        // echo "<div>propert $name not existed!</div>";
        if ($this->key_exists($key)) {
            return $this->{$key} = $this->param($key);
        } else {
            // notify programmer
        }
    }
}

<?php

namespace App\Utilities;

class Api
{

    public static function array_to_json($arr)
    {
        return json_encode($arr);
    }
    public static function json_to_array($json)
    {
        return json_decode($json, 1);
    }

    public static function respond($arr)
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        // header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
        // header("Access-Control-Max-Age: 3600");
        // header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        echo self::array_to_json($arr);
        exit();
    }
}

<?php

namespace App\Utilities;

class Content
{

    public static function excerpt($content, $wordcount, $postfix = '...')
    {
        $words = explode(' ', $content);
        return implode(' ', array_slice($words, 0, $wordcount)) . $postfix;
    }
}

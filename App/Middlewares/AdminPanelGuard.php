<?php

namespace App\Middlewares;

use App\Core\Request;
use App\Middlewares\Contract\BaseMiddleware;
use App\Services\Auth\Auth;

class AdminPanelGuard extends BaseMiddleware
{
    public function handle(Request $request)
    {
        if (!Auth::isAdminUser()) {
            die("Permission Denied!");
        }
    }
}

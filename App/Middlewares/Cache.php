<?php

namespace App\Middlewares;

use App\Core\Request;
use App\Middlewares\Contract\BaseMiddleware;
use App\Services\Auth\Auth;
use App\Services\Cache\Cache as CacheMan;

class Cache extends BaseMiddleware
{
    public function handle(Request $request)
    {
        if (!STATIC_CACHE_ENABLE or Auth::isLogin()) {
            return;
        }
        $cacheman = new CacheMan();
        // $cacheman->flushAll();
        $key = md5($request->uri);
        if ($cacheman->contains($key)) {
            echo $cacheman->get($key);
            die();
        }
    }
}

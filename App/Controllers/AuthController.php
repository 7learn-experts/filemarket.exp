<?php

namespace App\Controllers;

use App\Core\Request;
use App\Services\Auth\Auth;
use App\Services\View\View;
use App\Utilities\FlashMessage;

class AuthController
{


    public function index($request)
    {
        if (Auth::isLogin()) {
            FlashMessage::add("شما در سایت لاگین هستید", FlashMessage::INFO);
            Request::redirect($request->referer);
        }
        View::load('auth.index');
    }

    public function login($request)
    {
        // dd($request);
        Auth::login($request->email, $request->password);
        Request::redirect($request->backurl);
    }
    public function logout($request)
    {
        Auth::logout();
        Request::redirect($request->referer);
    }
    public function register($request)
    {
        $data = array(
            'name' => $request->fullname,
            'email' => $request->email,
            'password' => $request->password,
        );
        Auth::register($data);
        Request::redirect($request->backurl);
    }
}

<?php

namespace App\Controllers;

use App\Models\File;
use App\Services\Cache\Cache;
use App\Services\Content\FileProvider;
use App\Services\Content\FileProviderWithCache;
use App\Services\View\View;

class HomeController
{

    private $fileModel;
    private $cacheman;

    public function __construct()
    {
        $this->fileModel = new File();
        $this->cacheman = new Cache();
    }


    public function index($request)
    {
        $fileProvider = new FileProvider();
        $data = array(
            'files' => $fileProvider->get_files()
        );
        View::load('index', $data);
    }
}

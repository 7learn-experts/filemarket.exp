<?php

namespace App\Controllers;

use App\Core\Request;
use App\Models\Comment;
use App\Models\File;
use App\Models\Like;
use App\Services\View\View;

class FileController
{

    private $fileModel;
    private $commentModel;
    private $likeModel;

    public function __construct()
    {
        $this->fileModel = new File();
        $this->commentModel = new Comment();
        $this->likeModel = new Like();
    }


    public function single(Request $request)
    {
        $data = array(
            'file' => $this->fileModel->get('*', ['id' => $request->id]),
            'comments' => $this->commentModel->read('*', [
                'entity_type' => 'file',
                'entity_id' => $request->id
            ]),
            'likes' => $this->likeModel->count(
                [
                    'entity_type' => 'file',
                    'entity_id' => $request->id
                ]
            ),
            'alreadyLikes' => $this->likeModel->count([
                'entity_type' => 'file',
                'entity_id' => $request->id,
                'ip' => $request->ip
            ])
        );

        View::load('single', $data);
    }
}

<?php

namespace App\Controllers;

use App\Services\Payment\Online\OnlinePayment;

class PayController
{


    public function pay($request)
    {
        $whitelist = ['Online', 'Wallet'];
        if (!in_array($request->payType, $whitelist)) {
            die("Invalid Pay Request !");
        }

        $payTypeClass = str_replace('#PT#', $request->payType, "\App\Services\Payment\#PT#\#PT#Payment");;
        $payType = new $payTypeClass;

        // $payType->setGatway(...); // gateway injection (if user can select gateway)

        $payType->pay();
    }

    public function verify($request)
    {

        $payType = new OnlinePayment;
        $payType->verify();
    }
}

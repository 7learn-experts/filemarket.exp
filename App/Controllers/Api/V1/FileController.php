<?php

namespace App\Controllers\Api\V1;

use App\Core\Request;
use App\Models\File;
use App\Services\Content\FileProvider;
use App\Utilities\Api;

class FileController
{

    public $fileProvider;
    public $fileModel;

    public function __construct()
    {
        $this->fileProvider = new FileProvider();
        $this->fileModel = new File();
    }

    public function list(Request $request)
    {
        // check authorization first
        $files = $this->fileProvider->get_files();
        Api::respond($files);
    }

    public function delete(Request $request)
    {
        $json_str = file_get_contents("php://input");
        $where = Api::json_to_array($json_str);
        // check authorization first
        if (!isset($where['id']) or !is_numeric($where['id'])) {
            Api::respond([
                'status' => 'failed',
                'code' => 777,
                'message' => 'Invalid Parameters ...'
            ]);
        }

        if (sizeof($where) != 1) {
            Api::respond([
                'status' => 'failed',
                'code' => 778,
                'message' => 'Extra Parameters ...'
            ]);
        }

        $this->fileModel->delete($where);
        Api::respond([
            'status' => 'success',
            'code' => 200,
            'message' => 'Successfully deleted ...'
        ]);
    }
}


// json syntax
// array : [1,2,3,4]
// object: {"k1":"v1", ...}

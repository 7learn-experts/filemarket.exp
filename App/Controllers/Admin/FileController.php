<?php

namespace App\Controllers\Admin;

use App\Core\Request;
use App\Core\UploadedFile;
use App\Models\File;
use App\Services\View\View;
use App\Utilities\FlashMessage;

class FileController
{
    private $model;

    public function __construct()
    {
        $this->model = new File();
    }

    public function add($request)
    {
        View::load_from_base('admin.file.add', array(), 'layout-admin');
    }

    public function save($request)
    {

        $file = new UploadedFile('file');
        $file_thumb = new UploadedFile('file-thumb');
        $file_url = $file->save();
        $thumb_url = $file_thumb->save();
        $view_data = array();
        if ($file_url && $thumb_url) {
            $data = array(
                'type' => $request->fileType,
                'description' => $request->description,
                'title' => $request->title,
                'link' => $file_url,
                'thumb' => $thumb_url
            );
            $this->model->create($data);
            FlashMessage::add("Upload OK", FlashMessage::SUCCESS);
            // TODO: create all file_tag relations
        } else {
            FlashMessage::add("Upload Failed", FlashMessage::ERROR);

            $file->destroy();
            $file_thumb->destroy();
        }

        $view_data['formData'] = $request;

        View::load_from_base('admin.file.add', $view_data, 'layout-admin');
    }

    public function list($request)
    {
        $data = array(
            'files' => $this->model->read()
        );
        View::load_from_base('admin.file.list', $data, 'layout-admin');
    }

    public function delete(Request $request)
    {
        if ($request->key_exists('id')) {
            $this->model->delete(['id' => $request->id]);
        }
        Request::redirect('admin/file/list');
    }
}

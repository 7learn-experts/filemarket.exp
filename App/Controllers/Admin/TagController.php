<?php

namespace App\Controllers\Admin;

use App\Core\Request;
use App\Models\Tag;
use App\Services\View\View;

class TagController
{
    private $model;

    public function __construct()
    {
        $this->model = new Tag();;
    }

    public function add($request)
    {
        View::load_from_base('admin.tag.add', array(), 'layout-admin');
    }

    public function ajaxSave($request)
    {
        // sleep(20);
        if (!$request->isAjax()) {
            echo "Invalid Request !";
            die();
        }

        $tagAlreadyExists = $this->model->countBy('slug', $request->slug);
        if ($tagAlreadyExists) {
            echo "tag with slug '$request->slug' Already Exists";
            die();
        }

        if ($request->key_exists('title') && $request->key_exists('slug')) {
            $data = array('title' => $request->title, 'slug' => $request->slug);
            $this->model->create($data);
            echo "Tag Created !";
        }
    }

    public function list(Request $request)
    {

        $data = array(
            'tags' => $this->model->read()
        );
        View::load_from_base('admin.tag.list', $data, 'layout-admin');
    }

    public function fieldEdit(Request $request)
    {
        // sleep(1);
        // var_dump($request->params);
        list($tag_id, $tag_field) = explode('|', $request->id);
        $where = ['id' => $tag_id];
        $this->model->update(
            [$tag_field => $request->value],
            $where
        );

        echo $this->model->get($tag_field, $where);
    }

    public function delete(Request $request)
    {
        if ($request->key_exists('id')) {
            $this->model->delete(['id' => $request->id]);
        }
        Request::redirect('admin/tag/list');
    }
}

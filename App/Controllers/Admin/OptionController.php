<?php

namespace App\Controllers\Admin;

use App\Core\Request;
use App\Models\Option;
use App\Services\View\View;

class OptionController
{
    private $model;

    public function __construct()
    {
        $this->model = new Option();
    }

    public function add($request)
    {
        $data = array(
            'optionCats' => ['ui', 'seo', 'theme']     // dont hard code !
        );

        View::load_from_base('admin.option.add', $data, 'layout-admin');
    }

    public function ajaxSave($request)
    {
        // sleep(1);
        if (!$request->isAjax()) {
            echo "Invalid Request !";
            die();
        }

        $OptionAlreadyExists = $this->model->countBy('key', $request->key);
        if ($OptionAlreadyExists) {
            echo "Option with key '$request->key' Already Exists";
            die();
        }

        if (
            $request->key_exists('key') &&
            $request->key_exists('value') &&
            $request->key_exists('category')
        ) {
            $data = array('key' => $request->key, 'value' => $request->value, 'category' => $request->category);


            if (!$this->model->create($data)) {
                // var_dump($this->model->log());
                // var_dump($this->model->debug());
                die("an Error !");
            }
            echo "Option Created !";
        }
    }

    public function list(Request $request)
    {
        if ($request->key_exists('cat')) {
            $options = $this->model->read('*', ['category' => $request->cat]);
        } else {
            $options = $this->model->read();
        }

        $data = array(
            'options' => $options
        );
        View::load_from_base('admin.option.list', $data, 'layout-admin');
    }

    public function fieldEdit(Request $request)
    {
        // sleep(1);
        // var_dump($request->params);
        list($Option_id, $Option_field) = explode('|', $request->id);
        $where = ['id' => $Option_id];
        $this->model->update(
            [$Option_field => $request->value],
            $where
        );

        echo $this->model->get($Option_field, $where);
    }

    public function delete(Request $request)
    {
        if ($request->key_exists('id')) {
            $this->model->delete(['id' => $request->id]);
        }
        Request::redirect('admin/option/list');
    }
}

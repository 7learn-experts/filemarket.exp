<?php

namespace App\Controllers\Admin;

use App\Services\View\View;

class DashboardController
{


    public function index()
    {
        View::load_from_base('admin.dashboard', array(), 'layout-admin');
    }
}

<?php

namespace App\Controllers;

use App\Core\Request;
use App\Models\Like;

class LikeController
{

    private $likeModel;

    public function __construct()
    {
        $this->likeModel = new Like();
    }


    public function add(Request $request)
    {
        // TODO: validate request here
        $data = [
            'entity_type' => $request->entity,
            'entity_id' => $request->id,
            'ip' => $request->ip
        ];
        $where = [
            'entity_type' => $request->entity,
            'entity_id' => $request->id
        ];

        $alreadyExists = $this->likeModel->count($data);
        $result = array();

        $result['status'] = 'already';
        if (!$alreadyExists) {
            if ($this->likeModel->create($data)) {
                $result['status'] = 'created';
            } else {
                $result['status'] = 'error';
            }
        }
        $result['count'] = $this->likeModel->count($where);

        $className = "\\App\\Models\\" . ucfirst($request->entity);
        $entityModel = new $className;
        $entityModel->update(['likes' => $result['count']], ['id' => $request->id]);

        echo json_encode($result);
    }
}

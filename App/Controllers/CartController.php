<?php

namespace App\Controllers;

use App\Core\Request;
use App\Models\File;
use App\Services\Basket\Basket;
use App\Services\View\View;

class CartController
{

    public function index(Request $request)
    {
        $cart_items = Basket::items();

        $data = array(
            'cart_items' => $cart_items
        );

        View::load('cart.list', $data);
    }

    public function add(Request $request)
    {
        $fileModel = new File();
        $file = (object) $fileModel->get(['id', 'title', 'price', 'thumb'], ['id' => $request->id]);

        if ($file) {
            // add to cart
            Basket::add($file);
        }
        Request::redirect('cart');
    }

    public function remove(Request $request)
    {
        Basket::remove($request->id);
        Request::redirect('cart');
    }
}

<?php

namespace App\Enums;

class FileEnum
{

    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;
    const STATUS_SUSPENDED = 2;
    const STATUS_DELETED = 3;

    const TYPE_PDF = 1;
    const TYPE_VIDEO = 2;
    const TYPE_IMAGE = 3;
    const TYPE_XLS = 4;
    const type_titles = array(1 => "PDF", 2 => "ویدیو", 3 => "تصویر", 4 => "Excel");
}

<?php

namespace App\Models;

use App\Models\Contract\BaseModel;

class Tag extends BaseModel
{
    public static $table = 'tags';
}

<?php

namespace App\Models\Contract;

use stdClass;

class BaseModel implements CRUD
{
    public $fields;
    public static $conn;
    public static $table;
    public static $primary_key = 'id';
    public static $records_per_page = 10;

    public function __construct($id = null)
    {
        global $medoo;
        self::$conn = $medoo;
        $this->fields = array();
        if ($id) {
            return $this->find($id);
        }
    }


    public function create($data)
    {
        self::$conn->insert(static::$table, $data);
        return self::$conn->id();
    }

    public function findBy($field, $value)
    {
        return $this->read('*', [$field => $value]);
    }

    public function countBy($field, $value)
    {
        return count($this->findBy($field, $value));
    }

    public function read($columns = '*', $where = array(), $pagination = true, &$stats = null)
    {
        $records_per_page = $_GET['record_per_page'] ?? static::$records_per_page;

        if ($pagination) {
            $page = (isset($_GET['page']) and is_numeric($_GET['page'])
                and $_GET['page'] > 0) ? $_GET['page'] : 1;

            $start = ($page - 1) * $records_per_page;

            $where['LIMIT'] = [$start, $records_per_page];
        }

        $records = self::$conn->select(static::$table, $columns, $where);

        $stats = new \stdClass;
        $stats->total_records = $this->count();
        $stats->page_count = ceil($stats->total_records / $records_per_page);

        // $results = new \stdClass;
        // $results->records = array_of_objects($records);
        // $results->stats = $stats;
        // return $results;

        return array_of_objects($records);
    }


    public function get($columns = '*', $where = array())
    {
        // TODO: make sure about return value
        $data = self::$conn->get(static::$table, $columns, $where);
        if (is_null($data)) {
            return null;
        }
        return ($columns == '*') ? (object) $data : $data;
    }

    public function update($data, $where)
    {
        $result = self::$conn->update(static::$table, $data, $where);
        return $result->rowCount();
    }

    public function delete($where)
    {
        $result = self::$conn->delete(static::$table, $where);
        return $result->rowCount();
    }

    public function count($where = array())
    {
        return self::$conn->count(static::$table, $where);
    }

    public function query($query)
    {
        return self::$conn->query($query);
    }
    public function log()
    {
        return self::$conn->log();
    }

    public function debug()
    {
        return self::$conn->debug();
    }

    // public static function __callStatic($method, $parameters)
    // {
    //     var_dump($method, $parameters);
    //     // return (new static)->$method(...$parameters);
    // }
    // public static function __call($method, $parameters)
    // {
    //     var_dump("Call", $method, $parameters);
    // }

    public function find($id)
    {
        // TODO: make sure about return value
        $this->fields = self::$conn->get(static::$table, '*', [static::$primary_key => $id]);
        return $this;
    }

    public function __get($field)
    {
        if (is_null($this->fields)) {
            return;
        }
        return $this->fields[$field];
    }
    public function __set($field, $value)
    {
        if (is_null($this->fields)) {
            return;
        }
        $this->fields[$field] = $value;
    }
    public function save()
    {
        if (isset($this->fields[static::$primary_key])) {
            return $this->update($this->fields, [static::$primary_key => $this->fields[static::$primary_key]]);
        }
        return $this->create($this->fields);
    }
}

<?php

namespace App\Models;

use App\Models\Contract\BaseModel;

class File extends BaseModel
{
    public static $table = 'files';
    public static $records_per_page = 5;
}

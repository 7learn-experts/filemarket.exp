<?php

namespace App\Models;

use App\Models\Contract\BaseModel;

class User extends BaseModel
{
    public static $table = 'users';

    public function alreadyExists($email)
    {
        return $this->countBy('email', $email);
    }

    public function decreaseWallet($user_id, $amount)
    {
        $this->increaseWallet($user_id, -1 * $amount);
    }

    public function increaseWallet($user_id, $amount)
    {
        $where = ['id' => $user_id];
        $user = $this->get('*', $where);

        $this->update([
            'wallet' => $user->wallet + $amount
        ], $where);
    }
}

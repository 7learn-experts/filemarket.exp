<?php

namespace App\Models;

class ModelGenerator
{
    public $table_names;

    public function __construct()
    {
        global $medoo;
        $sql = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.`TABLES` where TABLE_SCHEMA = 'filemarket.exp'";
        $this->table_names = $medoo->query($sql)->fetchAll();
    }

    public function makeModels()
    {
        $modelTpl = '<?php

namespace App\Models;

use App\Models\Contract\BaseModel;

class #MODEL# extends BaseModel
{
    public static $table = "#TABLE#";
}

        ';

        foreach ($this->table_names as $table) {
            $content = str_replace(
                ["#MODEL#", "#TABLE#"],
                [ucfirst($table[0]), $table[0]],
                $modelTpl
            );
            file_put_contents("M-" . $table[0] . ".php", $content);
        }
    }
}

<?php

namespace App\Models;

use App\Models\Contract\BaseModel;

class Option extends BaseModel
{
    public static $table = 'options';
    public static $records_per_page = 3;
}

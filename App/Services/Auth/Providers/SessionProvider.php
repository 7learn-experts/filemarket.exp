<?php

namespace App\Services\Auth\Providers;

use App\Services\Auth\Contract\AuthProvider;
use App\Utilities\FlashMessage;

class SessionProvider extends AuthProvider
{
    const AUTH_KEY = 'auth';

    public function register(array $data)
    {

        // TODO: data validation
        if (!$this->isValidEmail($data['email'])) {
            FlashMessage::add("ایمیل وارد شده صحیح نیست", FlashMessage::WARNING);
            return false;
        }
        if (!$this->isStrongPassword($data['password'])) {
            FlashMessage::add("رمز عبور انتخاب شده باید شامل حروف و اعداد باشد.", FlashMessage::WARNING);
            return false;
        }

        if ($this->userModel->alreadyExists($data['email'])) {
            FlashMessage::add("کاربری با این ایمیل قبلا در سایت است.", FlashMessage::WARNING);
            return false;
        }
        $pass = $data['password'];
        $data['email'] = strtolower($data['email']);
        $data['password'] = $this->generateHash($data['password']);
        // insert user in db
        $user_id = $this->userModel->create($data);
        if ($user_id) {
            FlashMessage::add("ثبت نام شما انجام شد", FlashMessage::SUCCESS);
            // auto login here
            $this->login($data['email'], $pass);
            return $user_id;
        } else {
            FlashMessage::add("مشکلی در هنگام ثبت نام شما رخ داده است.", FlashMessage::ERROR);
            return false;
        }
    }

    public function login($email, $password)
    {
        $user = $this->userModel->get('*', ['email' => $email]);
        // wrong email
        if (is_null($user)) {
            FlashMessage::add("نام کاربری یا رمز عبور اشتباه است.", FlashMessage::ERROR);
            return false;
        }

        // wrong password
        if (!password_verify($password, $user->password)) {
            FlashMessage::add("نام کاربری یا رمز عبور اشتباه است.", FlashMessage::ERROR);
            return false;
        }

        $_SESSION[self::AUTH_KEY] = $user->id;
        FlashMessage::add("با موفقیت لاگین شدید", FlashMessage::SUCCESS);
        return true;
    }

    public function isLogin(): int
    {
        $user_id = $_SESSION[self::AUTH_KEY] ?? 0;
        return $user_id;
    }

    public function logout()
    {
        if (isset($_SESSION[self::AUTH_KEY])) {
            unset($_SESSION[self::AUTH_KEY]);
        }
        return true;
    }
}

<?php

namespace App\Services\Auth\Contract;

use App\Models\User;
use App\Services\Auth\Auth;

abstract class AuthProvider
{
    public static $instance = null;
    public $userModel;

    public static function instance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static(new User());
        }
        return static::$instance;
    }



    protected function __construct(User $userModel)
    {
        $this->userModel = $userModel;
    }

    public function isAdminUser($uid = null)
    {
        $user_id = $uid ?? Auth::isLogin();
        if (!$user_id) {
            return false;
        }
        $user_role = $this->userModel->get('role', ['id' => $user_id]);
        return ($user_role == 'admin');
    }

    public abstract function register(array $data);
    public abstract function login($email, $password);
    public abstract function isLogin(): int;
    public abstract function logout();

    // public abstract function reset_pass();





    public function generateHash($password)
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    public function generateRandomPassword()
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';

        //remember to declare $pass as an array
        $pass = array();
        $pass_length = rand(8, 16);
        for ($i = 0; $i < $pass_length; $i++) {
            $index = rand(0, strlen($chars) - 1);
            $pass[] = $chars[$index];
        }

        //turn the array into a string
        return implode($pass);
    }

    public function isValidEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public function isStrongPassword($password)
    {
        if (strlen($password) < 6) {
            return false;
        }

        // next policy here ... (alpha check)
        // another here ...     (special char check)

        return true;
    }
}

<?php

namespace App\Services\Content;

use App\Services\Cache\Cache;

class FileProviderWithCache
{
    private $fileProvider;
    private $cacheman;

    /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->fileProvider = new FileProvider();
        $this->cacheman = new Cache();
    }

    public function get_file($file_id)
    {

        $key = "object_file_$file_id";
        if ($this->cacheman->contains($key)) {
            return $this->cacheman->get($key);
        }
        $file = $this->fileProvider->get_file($file_id);
        $this->cacheman->set($key, $file);
        return $file;
    }


    public function get_files($params = array())
    {
        // $keys = md5(serialize($params)) // make unique ket if params exists
        $key = "object_allfiles";
        if ($this->cacheman->contains($key)) {
            return $this->cacheman->get($key);
        }
        $files = $this->fileProvider->get_files();
        $this->cacheman->set($key, $files);
        return $files;
    }
}

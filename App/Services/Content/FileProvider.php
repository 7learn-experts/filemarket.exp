<?php

namespace App\Services\Content;

use App\Models\File;

class FileProvider
{
    private $model;

    /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->model = new File();
    }

    public function get_file($file_id)
    {
        // sleep(3);
        return $this->model->get($file_id);
    }

    public function get_files($columns = '*', $where = array())
    {
        // sleep(3);

        $files = $this->model->read($columns, $where, true, $stats);
        return $files;
    }
}

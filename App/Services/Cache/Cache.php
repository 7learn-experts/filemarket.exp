<?php

namespace App\Services\Cache;

use App\Services\Cache\Conract\CacheContract;
use Exception;

class Cache /*  implements CacheContract */
{
    private $driver;
    protected $cacheEnabled = 0;
    const LIFETIME = 3600;
    const NAMESPACE = '7lc_';

    protected static $instance = null;
    public static function instance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function contains($key)
    {
        return $this->cacheEnabled && $this->driver->contains($key);
    }

    public function set($key, $value, $lifeTime = null)
    {
        if (!$this->cacheEnabled) {
            return;
        }
        return $this->driver->save($key, $value, $lifeTime ?? self::LIFETIME);
    }

    public function get($key)
    {
        if (!$this->cacheEnabled) {
            return;
        }
        return $this->driver->fetch($key);
    }
    public function delete($key)
    {
        if (!$this->cacheEnabled) {
            return;
        }
        return $this->driver->delete($key);
    }

    public function flushAll()
    {
        if (!$this->cacheEnabled) {
            return;
        }
        return $this->driver->flushAll();
    }

    // docs for change driver :
    // https://www.doctrine-project.org/projects/doctrine-orm/en/current/reference/caching.html
    public function __construct()
    {
        $this->initiateDriver('redis');
    }

    public function initiateDriver($driver)
    {

        switch ($driver) {
            case 'redis':
                try {

                    $redis = new \Redis();
                    $redis->connect('127.0.0.1', 6379);
                    $this->cacheEnabled = $redis->ping();
                    $this->driver = new \Doctrine\Common\Cache\RedisCache();
                    $this->driver->setRedis($redis);
                    $this->driver->setNamespace(self::NAMESPACE);
                } catch (\Throwable $th) {
                    $this->cacheEnabled = 0;
                    var_dump($th->getMessage());
                }
                break;
                // another drivers here
            default:
                // default driver here
                break;
        }
    }
}

<?php

namespace App\Services\View;

use App\Services\Cache\Cache;
use App\Utilities\Option;

class View
{
    // load from base 
    public static function load_from_base($view, $data = array(), $layout = null)
    {
        $cacheman = new Cache();
        $key = md5($_SERVER['REQUEST_URI']);

        // sleep(3);

        ob_start();
        $view = str_replace('.', DIRECTORY_SEPARATOR, $view);
        $full_view_path = BASE_VIEW_PATH . "$view.php";

        if (file_exists($full_view_path) && is_readable($full_view_path)) {
            ob_start();
            extract($data);
            include_once $full_view_path;
            $view = ob_get_clean();
            if (is_null($layout)) {
                echo $view;
            } else {
                $layout_full_view_path = BASE_VIEW_PATH . "layouts" . DIRECTORY_SEPARATOR . "$layout.php";
                include_once $layout_full_view_path;
            }
        } else {
            echo "Error: view not exists!";
        }

        $rendered_html = ob_get_clean();
        if (STATIC_CACHE_ENABLE) {
            $cacheman->set($key, $rendered_html);
        }
        echo $rendered_html;
    }

    // load from themes
    public static function loadError($view, $data = array(), $layout = null)
    {
        // load error pages without cache
    }

    public static function load($view, $data = array(), $layout = null)
    {

        $active_theme = Option::get('active_theme');
        // check theme exists !
        self::load_from_base($active_theme . "." . $view, $data, $layout);
    }



    // load from base 
    public static function renderFromBase($view, $data = array(), $layout = null)
    {
        ob_start();
        self::load_from_base($view, $data, $layout);
        return ob_get_clean();
    }
    public static function render($view, $data = array(), $layout = null)
    {
        ob_start();
        self::load($view, $data, $layout);
        return ob_get_clean();
    }
}

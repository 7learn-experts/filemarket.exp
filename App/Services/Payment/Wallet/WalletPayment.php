<?php

namespace App\Services\Payment\Wallet;

use App\Models\User;
use App\Services\Auth\Auth;
use App\Services\Basket\Basket;
use App\Services\Payment\Contracts\PaymentMethod;

class WalletPayment implements PaymentMethod
{


    public function pay()
    {
        $user_id = Auth::isLogin();
        if (!$user_id) {
            dd("You Are not Login");
        }
        $model = new User();
        $user = $model->get('*', ['id' => $user_id]);
        if ($user->wallet < Basket::total()) {
            dd('wallet amount is low');
        }
        $order_items = Basket::items();

        echo "<div>دانلود کنید:";
        foreach ($order_items as $item) {

            echo "<a href='$item->thumb'>$item->title</a>";
        }
        echo "</div>";
        $model->decreaseWallet($user->id, Basket::total());
        Basket::reset();

        // check wallet amount
        // check basket price
        // if(wallet<basket) msg: wallet amount is not enough 
        //////// start transaction
        // insert payment record
        // apply order items for user 
        // basket->reset();
        // wallet->decrease(basket.price)
        //////// commit transaction

    }
}

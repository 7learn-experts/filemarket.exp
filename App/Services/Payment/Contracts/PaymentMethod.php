<?php

namespace App\Services\Payment\Contracts;

interface PaymentMethod
{
    public function pay();
}

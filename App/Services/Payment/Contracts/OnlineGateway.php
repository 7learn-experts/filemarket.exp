<?php

namespace App\Services\Payment\Contracts;

interface OnlineGateway
{
    // from app to gateway
    public function payRequest($params);
    // form gateway to app
    public function verifyRequest($params);
}

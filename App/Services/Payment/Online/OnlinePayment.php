<?php

namespace App\Services\Payment\Online;

use App\Core\Request;
use App\Services\Basket\Basket;
use App\Services\Payment\Contracts\OnlineGateway;
use App\Services\Payment\Contracts\PaymentMethod;
use App\Services\Payment\Online\Gateway\ZarinPalGateway;

class OnlinePayment implements PaymentMethod
{
    public $gateway;


    public function __construct()
    {
        $this->gateway = new ZarinPalGateway();
    }

    public function setGateway(OnlineGateway $gateway)
    {
        $this->gateway = $gateway;
    }

    public function pay()
    {
        $resnum = bin2hex(random_bytes(2)) . time();
        // basket to order here (order with unique resnum : $resnum )

        $params = array(
            'amount' => Basket::total(),
            'description' => "پرداخت تستی فایل مارکت",
            'email' => "test@gmail.com",
            'mobile' => '09777777777',
            'callback_url' => site_url('payment-verification?resnum=' . $resnum)
        );
        $this->gateway->payRequest($params);
    }


    public function verify()
    {
        // basket amount here for pay value validation
        $params = array('amount' => Basket::total());

        $payment_ok = $this->gateway->verifyRequest($params);
        if (!$payment_ok) {
            return false;
        }
        // at this line payment is ok !
        echo "پرداخت موفق بود";

        // get order by resnum reterned by gateway
        // register order for users
        // clear basket

        $order_items = Basket::items();

        echo "<div>your Files are ready:";
        foreach ($order_items as $item) {
            echo "<a href='$item->thumb'>$item->title</a>";
        }
        echo "</div>";

        Basket::reset();
    }
}

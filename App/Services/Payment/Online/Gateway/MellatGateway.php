<?php

namespace App\Services\Payment\Online\Gateway;

use App\Services\Payment\Contracts\OnlineGateway;

class MellatGateway implements OnlineGateway
{

    // from app to gateway
    public function payRequest($params)
    { }


    // form gateway to app
    public function verifyRequest($params)
    { }
}

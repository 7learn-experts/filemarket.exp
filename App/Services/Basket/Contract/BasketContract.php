<?php

namespace App\Services\Basket\Contract;

interface BasketContract
{

    public function add($item);
    public function remove(int $item_id);
    public function total();    // total price
    public function reset();
    public function items();
    public static function count();
}

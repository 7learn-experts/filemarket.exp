<?php
define('BASE_URL', 'http://filemarket.exp/');

define('BASE_PATH', dirname(__DIR__) . DIRECTORY_SEPARATOR);

define('STORAGE_PATH', BASE_PATH . 'storage' . DIRECTORY_SEPARATOR);


define('STATIC_CACHE_ENABLE', 0);

define('SUB_DIRECTORY', '');

define('BASE_VIEW_PATH', BASE_PATH . 'views' . DIRECTORY_SEPARATOR);

define('DEFAULT_THEME', '2019');

define('IS_DEV_MODE', 1);
define('SANITIZE_ALL_DATA', 0);
define('GLOBAL_MIDDLEWARES', 'IEBlocker|Cache');

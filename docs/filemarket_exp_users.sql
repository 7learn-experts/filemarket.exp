-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2019 at 06:56 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `filemarket.exp`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_log`
--

CREATE TABLE `access_log` (
  `id` int(11) NOT NULL,
  `ip` varchar(64) NOT NULL,
  `referer` varchar(255) CHARACTER SET utf8 NOT NULL,
  `agent` varchar(128) NOT NULL,
  `uri` varchar(128) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `calls`
--

CREATE TABLE `calls` (
  `id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `file_id` int(11) NOT NULL,
  `agent` varchar(128) NOT NULL,
  `user_mobile` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `entity_type` varchar(64) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `author` varchar(128) CHARACTER SET utf8 NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(16) NOT NULL,
  `likes` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `entity_type`, `entity_id`, `author`, `content`, `parent`, `ip`, `likes`, `created_at`) VALUES
(1, 'file', 8, 'لقمان', 'عالی بود مرسی', 0, '128.15.2.2', 13, '2019-10-01 19:58:06'),
(2, 'file', 8, 'علی بردبار', 'لورم ایپسوم متن ساختگی با تولید...', 0, '128.15.2.2', 12, '2019-10-01 19:58:31');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `thumb` varchar(512) DEFAULT NULL,
  `link` varchar(255) NOT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `likes` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `type`, `title`, `description`, `thumb`, `link`, `price`, `created_at`, `likes`) VALUES
(2, 1, 'نمونه فایل', 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. ', 'thumb-url.png', 'link-rrl.png', 77770, '2019-09-14 17:53:39', 0),
(3, 1, 'دیتابیس آسمان خراش ها', 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. ', 'http://filemarket.exp/storage/thumb-phpjavan.jpg', 'http://filemarket.exp/storage/file-7skyscrapers Diagram.pdf', 77770, '2019-09-14 19:52:56', 0),
(4, 2, 'ssss', 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. ', 'http://filemarket.exp/storage/thumb-salespic.jpg', 'http://filemarket.exp/storage/file-holookar.sql', 77770, '2019-09-14 19:56:44', 0),
(5, 2, 'fff', 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. ', 'http://filemarket.exp/storage/201909/phpjavan-5a2b.jpg', 'http://filemarket.exp/storage/201909/teachline-aa07.sql', 77770, '2019-09-17 18:45:06', 0),
(6, 1, 'محصول شماره 1', 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. ', 'http://filemarket.exp/storage/201909/teachline-1dc0.sql', 'http://filemarket.exp/storage/201909/salespic-d97f.jpg', 77770, '2019-09-17 19:15:17', 0),
(7, 1, 'dfgd', 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. ', 'http://filemarket.exp/storage/201909/salespic-a0b2.jpg', 'http://filemarket.exp/storage/201909/teachline-f86d.sql', 77770, '2019-09-17 19:44:05', 0),
(8, 1, 'dfgd', 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. ', 'http://filemarket.exp/storage/201909/salespic-1d41.jpg', 'http://filemarket.exp/storage/201909/teachline-164b.sql', 77770, '2019-09-17 19:48:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `file_tags`
--

CREATE TABLE `file_tags` (
  `id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `entity_type` varchar(64) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `entity_type`, `entity_id`, `ip`, `created_at`) VALUES
(39, 'comment', 1, '127.0.0.2', '2019-10-01 20:27:19'),
(40, 'comment', 1, '127.0.0.2', '2019-10-01 20:27:50'),
(41, 'comment', 1, '127.0.0.2', '2019-10-01 20:27:57'),
(42, 'comment', 1, '127.0.0.2', '2019-10-01 20:28:57'),
(43, 'comment', 1, '127.0.0.2', '2019-10-01 20:29:43'),
(44, 'comment', 1, '127.0.0.2', '2019-10-01 20:30:01'),
(45, 'comment', 1, '127.0.0.2', '2019-10-01 20:30:07'),
(46, 'comment', 1, '127.0.0.2', '2019-10-01 20:30:34'),
(47, 'comment', 1, '127.0.0.2', '2019-10-01 20:30:37'),
(48, 'comment', 2, '127.0.0.2', '2019-10-01 20:30:42'),
(49, 'comment', 2, '127.0.0.2', '2019-10-01 20:30:44'),
(50, 'comment', 2, '127.0.0.2', '2019-10-01 20:30:44'),
(51, 'comment', 2, '127.0.0.2', '2019-10-01 20:30:44'),
(52, 'comment', 2, '127.0.0.2', '2019-10-01 20:30:44'),
(53, 'comment', 2, '127.0.0.2', '2019-10-01 20:30:47'),
(54, 'comment', 2, '127.0.0.2', '2019-10-01 20:30:47'),
(55, 'comment', 2, '127.0.0.2', '2019-10-01 20:30:47'),
(56, 'comment', 2, '127.0.0.2', '2019-10-01 20:30:47'),
(57, 'comment', 2, '127.0.0.2', '2019-10-01 20:30:47'),
(58, 'comment', 2, '127.0.0.2', '2019-10-01 20:30:48'),
(59, 'comment', 1, '127.0.0.2', '2019-10-01 20:30:49'),
(60, 'comment', 1, '127.0.0.2', '2019-10-01 20:30:49'),
(61, 'comment', 1, '127.0.0.2', '2019-10-01 20:30:58'),
(62, 'comment', 1, '127.0.0.1', '2019-10-01 20:31:47'),
(63, 'comment', 2, '127.0.0.1', '2019-10-01 20:31:51'),
(64, 'file', 8, '127.0.0.1', '2019-10-01 20:32:39');

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` int(11) NOT NULL,
  `key` varchar(256) NOT NULL,
  `value` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `category` enum('ui','theme','seo') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `key`, `value`, `category`) VALUES
(1, 'active_theme', '2019', 'ui'),
(2, 'site-title', 'اولین پروژه من', 'ui'),
(3, 'seo-description', 'اولین توضیح پروژه من', 'ui'),
(4, 'dehghani', 'mokhlese', 'ui'),
(5, 'ali', 'hasan', 'ui'),
(6, 'color', 'red', 'theme');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `slug` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `title`, `slug`) VALUES
(1, 'تگ دو', 'tg2'),
(2, 'طبیعت', 'nature'),
(3, 'زرد', 'ylow'),
(17, 'قرمز', 'red'),
(20, 'dddddd', 'fffffffff');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(256) CHARACTER SET utf8 NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(256) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `role` enum('user','admin','agent','') NOT NULL DEFAULT 'user',
  `wallet` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`, `wallet`, `created_at`) VALUES
(3, 'Loghman Avand', 'loghman@avand.com', '$2y$10$UJI050PSmFJdVyFdz1LjFu.mK3giSre6jytdXuXyh9zn6QgSKn6KO', 'user', 9844459, '2019-10-12 18:07:43'),
(4, 'Ali Bordbar', 'ali@bordbar.com', '$2y$10$1ZLOn7VOcqfwb/SXEDgUouQBn1j.OTXiuqgcPkHwJe113YO1fmKJO', 'user', 0, '2019-10-12 18:08:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_log`
--
ALTER TABLE `access_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `calls`
--
ALTER TABLE `calls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `file_tags`
--
ALTER TABLE `file_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `file_id` (`file_id`),
  ADD KEY `tag_id` (`tag_id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_log`
--
ALTER TABLE `access_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `calls`
--
ALTER TABLE `calls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `file_tags`
--
ALTER TABLE `file_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

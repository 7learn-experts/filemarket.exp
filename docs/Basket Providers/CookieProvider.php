<?php


namespace App\Services\Basket\Providers;

use App\Services\Basket\Contract\BasketContract;

class CookieProvider implements BasketContract
{
    protected static $instance = null;
    const COOKIE_Name = 'cart';

    public static function instance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function total()
    {
        $items = [];
        if (isset($_COOKIE[self::COOKIE_Name])) {
            $basket_items = str_replace('\\', '', $_COOKIE[self::COOKIE_Name]);
            if (is_serialized($basket_items)) {
                $items = $this->decode_cookie_value($basket_items);
            }
        }
        return $items;
    }

    public function add($course)
    {
        if (!is_object($course)) {
            return null;
        }
        $items     = $this->total();
        $course_id = $course->sid;
        if ($items == null || !in_array($course_id, $items)) {
            $items[]     = $course_id;
            $basket_item = $this->encode_cookie_value($items);
            $this->set_basket_cookies($basket_item);
        }
        return $items;
    }

    public function check_cookies($item_index)
    {
        if (empty($item_index)) {
            return null;
        }
        $items = $this->total();
        if (is_array($items) && !array_key_exists($item_index, $items)) {
            return true;
        }
        return false;
    }

    public function set_basket_cookies($values)
    {
        setcookie(self::COOKIE_Name, $values, time() + 24 * 6400, "/");
    }

    public function remove($basket_id)
    {
        if ($basket_id == null) {
            return null;
        }
        $items = $this->total();
        if (is_array($items) && in_array($basket_id, $items)) {
            $key = array_search($basket_id, $items);
            unset($items[$key]);
            $new_basket_items = $this->encode_cookie_value($items);
            $this->set_basket_cookies($new_basket_items);
        }
        return $items;
    }

    public function encode_cookie_value($items)
    {
        return serialize(json_encode($items));
    }

    public function decode_cookie_value($items)
    {
        return json_decode(unserialize($items), true);
    }

    public function itemSubTotal($item)
    { }

    public function reset()
    {
        setcookie(self::COOKIE_Name, '', time() - 3600);
    }
}

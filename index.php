<?php

use App\Core\Request;
use App\Models\File;
use App\Models\Tag;
use App\Models\User;
use App\Services\Cache\Cache;

session_start();

include "bootstrap/constants.php";
include "vendor/autoload.php";
include "helpers/global.php";
include "bootstrap/init.php";



App\Services\Router\Router::start();

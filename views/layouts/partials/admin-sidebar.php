<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">

    <div class="slimscroll-menu">

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                <li>
                    <a href="javascript: void(0);" class="waves-effect">
                        <i class="remixicon-stack-line"></i>
                        <span>داشبورد </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="<?= admin_url('') ?>">داشبورد</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="waves-effect">
                        <i class="remixicon-stack-line"></i>
                        <span>فایل ها </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="<?= admin_url('file/add') ?>">افزودن</a>
                            <a href="<?= admin_url('file/list') ?>">لیست فایل ها</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);" class="waves-effect">
                        <i class="remixicon-stack-line"></i>
                        <span>تگ ها </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="<?= admin_url('tag/add') ?>">افزودن</a>
                            <a href="<?= admin_url('tag/list') ?>">لیست تگ ها</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="waves-effect">
                        <i class="remixicon-stack-line"></i>
                        <span>تنظیمات</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="<?= admin_url('option/add') ?>">افزودن</a>
                            <a href="<?= admin_url('option/list') ?>">همه تنظیمات</a>
                            <a href="<?= admin_url('option/list?cat=ui') ?>">تنظیمات ui</a>
                            <a href="<?= admin_url('option/list?cat=seo') ?>">تنظیمات seo</a>
                            <a href="<?= admin_url('option/list?cat=theme') ?>">تنظیمات قالب</a>
                        </li>
                    </ul>
                </li>

            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
<div class="content">

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">افزودن آپشن جدید</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-12">
                            <div class="p-2">
                                <form action="<?= admin_url('option/ajaxSave') ?>" class="form-horizontal ajax-form" method="POST">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="file_type">دسته بندی</label>
                                        <div class="col-sm-10">

                                            <select name="category" class="form-control" id="category">

<?php foreach($optionCats as $cat): ?>
                                            <option value="<?= $cat ?>"><?= $cat ?></option>
<?php endforeach;?>
                                            </select>

                                        </div>
                                    </div>                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="simpleinput">Option Key</label>
                                        <div class="col-sm-10">
                                            <input type="text" id="simpleinput" name="key" class="form-control" placeholder="ex: active_theme">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="example-email">مقدار</label>
                                        <div class="col-sm-10">
                                            <input type="text" id="example-email" name="value" class="form-control" placeholder="ex: 2077">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">افزودن</button>
                                    <div class="result"></div>
                                </form>
                            </div>
                        </div>

                    </div>
                    <!-- end row -->

                </div> <!-- end card-box -->
            </div><!-- end col -->
        </div>

    </div> <!-- container -->

</div>
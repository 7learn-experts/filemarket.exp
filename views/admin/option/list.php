<div class="content">

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">مدیریت تگ ها</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="table-responsive">
                <table class="table table-striped mb-0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Option</th>
                            <th>Value</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($options as $option) : ?>
                            <tr>
                                <th scope="row"><?= $option->id ?></th>
                                <td class='optionf jie' id='<?= $option->id ?>|key'><?= $option->key ?></td>
                                <td class='optionf jie' id='<?= $option->id ?>|value'><?= $option->value ?></td>
                                <td><a href="<?= admin_url('option/delete?id=' . $option->id) ?>"><i class="remixicon-delete-bin-2-line"></i></a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div> <!-- container -->

</div>
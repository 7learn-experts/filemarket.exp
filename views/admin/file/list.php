<div class="content">

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">مدیریت فایل ها</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="table-responsive">
                <table class="table table-striped mb-0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>نوع</th>
                            <th>عنوان</th>
                            <th>توضیح</th>
                            <th>بندانگشتی</th>
                            <th>لینک</th>
                            <th>تاریخ آپلود</th>
                            <th>عملیات</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($files as $file) : ?>
                            <tr>
                                <th scope="row"><?= $file->id ?></th>
                                <td><?= $file->type ?></td>
                                <td><?= $file->title ?></td>
                                <td><?= $file->description ?></td>
                                <td><?= $file->thumb ?></td>
                                <td><?= $file->link ?></td>
                                <td><?= $file->created_at ?></td>
                                <td><a href="<?= admin_url('file/delete?id=' . $file->id) ?>"><i class="remixicon-delete-bin-2-line"></i></a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div> <!-- container -->

</div>
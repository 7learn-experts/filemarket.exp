<div class="content">

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">افزودن فایل</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-12">
                            <div class='<?= $msgType ?? '' ?>'><?= $msg ?? '' ?></div>
                            <div class="p-2">
                                <form action="<?= admin_url('file/save') ?>" class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="example-fileinput1">آپلود فایل</label>
                                        <div class="col-sm-10">
                                            <input type="file" name="file" class="form-control" id="example-fileinput1">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="simpleinput">عنوان فایل</label>
                                        <div class="col-sm-10">
                                            <input type="text" id="simpleinput" name="title" class="form-control" value="<?= $formData->title ?? '' ?>" placeholder="مثلا: طبیعت">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="file_type">نوع فایل</label>
                                        <div class="col-sm-10">

                                            <select name="fileType" class="form-control" id="file_type">
                                                <?php
                                                foreach (\App\Enums\FileEnum::type_titles as $key => $value) : ?>
                                                    <option value="<?= $key ?>"><?= $value ?></option>
                                                <?php endforeach; ?>

                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="example-textarea">توضیح</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="example-textarea" name="description" rows="5"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="example-fileinput2">تصویر بندانگشتی</label>
                                        <div class="col-sm-10">
                                            <input type="file" name="file-thumb" class="form-control" id="example-fileinput2">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">تگ ها</label>
                                        <div class="col-sm-10">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">با کاما جدا کنید: </span>
                                                </div>
                                                <input type="text" class="form-control" name="tags" placeholder="tags" aria-label="tags" aria-describedby="basic-addon1">
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">افزودن</button>
                                </form>
                            </div>
                        </div>

                    </div>
                    <!-- end row -->


                </div> <!-- container -->

            </div>
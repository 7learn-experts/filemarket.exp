<div class="content">

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">افزودن تگ</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-12">
                            <div class="p-2">
                                <form action="<?= admin_url('tag/ajaxSave') ?>" class="form-horizontal ajax-form" method="POST">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="simpleinput">عنوان برچسب</label>
                                        <div class="col-sm-10">
                                            <input type="text" id="simpleinput" name="title" class="form-control" placeholder="مثلا: طبیعت">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="example-email">نامک (انگلیسی)</label>
                                        <div class="col-sm-10">
                                            <input type="text" id="example-email" name="slug" class="form-control" placeholder="مثلا: nature">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">افزودن</button>
                                    <div class="result"></div>
                                </form>
                            </div>
                        </div>

                    </div>
                    <!-- end row -->

                </div> <!-- end card-box -->
            </div><!-- end col -->
        </div>

    </div> <!-- container -->

</div>
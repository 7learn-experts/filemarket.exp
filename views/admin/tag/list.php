<div class="content">

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">مدیریت تگ ها</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="table-responsive">
                <table class="table table-striped mb-0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>نامک</th>
                            <th>عنوان</th>
                            <th>عملیات</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($tags as $tag) : ?>
                            <tr>
                                <th scope="row"><?= $tag->id ?></th>
                                <td class='tagf jie' id='<?= $tag->id ?>|slug'><?= $tag->slug ?></td>
                                <td class='tagf jie' id='<?= $tag->id ?>|title'><?= $tag->title ?></td>
                                <td><a href="<?= admin_url('tag/delete?id=' . $tag->id) ?>"><i class="remixicon-delete-bin-2-line"></i></a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div> <!-- container -->

</div>
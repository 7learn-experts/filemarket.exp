<nav class='main-nav'>
    <ul>
        <li>
            <a href="

<?= site_url() ?>">
                <img src="<?= theme_url('assets/img/logo.png') ?>" class="logo">
                صفحه اصلی
            </a>
        </li>
        <li><a href="<?= site_url('about') ?>">درباره ما</a></li>
        <?php if (!\App\Services\Auth\Auth::isLogin()) : ?>
            <li><a href="<?= site_url('auth') ?>">ورود/عضویت</a></li>
        <?php else : ?>
            <li><a href="<?= site_url('auth/logout') ?>">خروج</a></li>
        <?php endif; ?>

    </ul>
    <a class='cart-icon' href="<?= site_url('cart') ?>">
        <span class="fas fa-shopping-cart"></span>
        <span class='count'><?= \App\Services\Basket\Basket::count() ?></span>
    </a>
</nav>
<?php \App\Utilities\FlashMessage::show_messages(); ?>
<style>
    .flash-messages {
        background: #fff;
        position: fixed;
        bottom: 0;
        padding: 10px 20px;
    }

    nav.main-nav {
        position: relative;
        background: #fff;
        height: 50px;
        padding: 0;
        margin: 0;
    }

    a.cart-icon {
        position: absolute;
        top: 4px;
        left: 9px;
        color: #7b7b7b;
        font-size: 24px;
        text-decoration: none;
    }

    a.cart-icon span.count {
        display: inline-block;
        background: #d1ba00;
        height: 12px;
        width: 10px;
        line-height: 15px;
        border-radius: 100px;
        color: #fff;
        padding: 5px;
        font-size: 16px;
        position: absolute;
        left: 19px;
    }

    nav.main-nav ul {
        direction: rtl;
        list-style: none;
    }

    nav.main-nav ul li {
        display: inline-block;
        line-height: 50px;
        float: inherit;
        width: inherit;
    }

    nav.main-nav ul li a {
        display: inline-block;
        line-height: 50px;
        padding-right: 10px;
        padding-left: 10px;
        text-decoration: none;
        font-family: Sahel;
        color: #0052bf;
    }

    nav.main-nav ul li a:hover {
        background: #f7f7f7;
        color: #555;

    }

    img.logo {
        width: 32px;
        vertical-align: -10px;
    }

    i.fas.fa-cart-plus {
        position: absolute;
        right: 10px;
        bottom: 143px;
        color: #4CAF50;
        font-size: 20px;
    }

    .cart-buttons {
        text-align: center;
    }

    .btn {
        text-decoration: none;
        background: #f7f7f7;
        padding: 7px 20px;
        border-radius: 5px;
        margin: 10px;
        color: #333;
        border: 0;
        font-size: 17px;
    }

    .btnBlue {
        color: #fff;
        background: #0063e6;
    }

    .btnGreen {
        color: #fff;
        background: green;

    }

    .comments.pay-type {
        text-align: center;
    }

    label:hover {
        cursor: pointer;
        background: #ecf6ff;
    }

    label {
        direction: rtl;
        text-align: right;
        background: #fff;
        display: inline-block;
        padding: 5px 30px;
        border-radius: 5px;
    }
</style>
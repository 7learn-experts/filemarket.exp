<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Login, Register form</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel='stylesheet' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
  <link rel="stylesheet" href="<?= asset('css/auth.css') ?>">
</head>

<body>
  <!-- partial:index.partial.html -->

  <div class="login-box">
    <div class="lb-header">
      <a href="#" class="active" id="login-box-link">Login</a>
      <a href="#" id="signup-box-link">Sign Up</a>
    </div>
    <div class="social-login">
      <a href="#">
        <i class="fa fa-facebook fa-lg"></i>
        Login in with facebook
      </a>
      <a href="#">
        <i class="fa fa-google-plus fa-lg"></i>
        log in with Google
      </a>
    </div>
    <form class="email-login" action="<?= site_url('auth/login'); ?>" method='POST'>
      <div class="u-form-group">
        <input type="email" placeholder="Email" name='email' />
      </div>
      <div class="u-form-group">
        <input type="password" placeholder="Password" name='password' />
      </div>
      <div class="u-form-group">
        <button>Log in</button>
      </div>
      <div class="u-form-group">
        <a href="#" class="forgot-password">Forgot password?</a>
      </div>
      <input type="hidden" name='backurl' value="<?= $_SERVER['HTTP_REFERER'] ?>" />
    </form>
    <form class="email-signup" action="<?= site_url('auth/register'); ?>" method='POST'>
      <div class="u-form-group">
        <input type="text" placeholder="Your Name" name='fullname' />
      </div>
      <div class="u-form-group">
        <input type="email" placeholder="Email" name='email' />
      </div>
      <div class="u-form-group">
        <input type="password" placeholder="Password" name='password' />
      </div>
      <div class="u-form-group">
        <button>Sign Up</button>
      </div>
      <input type="hidden" name='backurl' value="<?= $_SERVER['HTTP_REFERER'] ?>" />

    </form>
  </div>
  <?php \App\Utilities\FlashMessage::show_messages(); ?>

  <!-- partial -->
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src="<?= asset('js/auth.js') ?>"></script>

</body>

</html>
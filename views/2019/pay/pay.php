<?php

use App\Utilities\Content; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>پرداخت</title>
    <link rel="stylesheet" href="<?= theme_url('assets/css/single.css') ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">

</head>

<body>
    <?php
    include theme_path("view/navbar.php");
    ?>


    <div class='comments'>
        result ...
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- partial -->
    <script src="<?= theme_url('assets/js/single.js') ?>"></script>
    <script src="<?= theme_url('assets/js/common.js') ?>"></script>

</body>

</html>
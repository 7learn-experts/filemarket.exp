$(document).ready(function () {

    $(".like-btn").click(function (e) {
        e.preventDefault();
        var action = 'http://filemarket.exp/like/add';
        var btn = $(this);
        var countBox = btn.parent().find('.count');
        countBox.html('...');
        $.ajax({
            type: 'POST',
            url: action,
            data: { entity: btn.attr('data-type'), id: btn.attr('data-id') },
            timeout: 10000,
            success: function (response) {
                response = JSON.parse(response);
                countBox.html(response.count);
                if (response.status != 'error') {
                    btn.removeClass('far').addClass('fas');
                }
            },
            error: function () {
                resultBox.html("Error!");
            }
        });

    });
});
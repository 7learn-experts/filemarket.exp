<?php

use App\Utilities\Content; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>


        <?= $file->title ?></title>
    <link rel="stylesheet" href="<?= theme_url('assets/css/single.css') ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">

</head>

<body>
    <?php
    include theme_path("view/navbar.php")
    ?>
    <?php
    // Scope problem
    // include_theme_path("view/navbar.php") 

    ?>

    <div id="make-3D-space">
        <div id="product-card">
            <div id="product-front">
                <div class="shadow"></div>
                <img src="fbg.png" alt="" />
                <div class="image_overlay"></div>
                <div id="view_details">
                    <a href="<?= $file->link ?>" download>Download</a>
                </div>
                <div class="stats">
                    <div class="stats-container">
                        <span class="product_price">Free</span>
                        <span class="product_name"><?= $file->title ?></span>
                        <p><?= Content::excerpt($file->description, 6) ?></p>

                        <div class="product-options">
                            <strong>تگ1، تگ2</strong>
                        </div>
                        <div class="like">
                            <i class="like-btn <?= ($alreadyLikes) ? 'fas' : 'far' ?> fa-heart" data-type='file' data-id='<?= $file->id ?>'></i>
                            <span class="count"><?= $likes ?></span>
                        </div>
                        <div class="cart-add">
                            <a href="<?= site_url("cart/add?id=$file->id") ?>">
                                <i class="fas fa-cart-plus"></i>
                            </a>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class='comments'>
        <?php foreach ($comments as $comment) : ?>

            <div class='comment'>
                <div class='comment-info'>
                    <div class='commenter'><?= $comment->author ?></div>
                    <div class="like">
                        <i class="like-btn <?= (\App\Utilities\Like::ipAlreadyLikes('comment', $comment->id)) ? 'fas' : 'far' ?> fa-heart" data-type='comment' data-id='<?= $comment->id ?>'></i>
                        <span class="count"><?= $comment->likes ?></span>
                    </div>
                </div>
                <div class='comment-content'><?= $comment->content ?></div>
            </div>
        <?php endforeach; ?>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- partial -->
    <script src="<?= theme_url('assets/js/single.js') ?>"></script>
    <script src="<?= theme_url('assets/js/common.js') ?>"></script>

</body>

</html>
<?php

use App\Utilities\Content; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>سبد خرید</title>
    <link rel="stylesheet" href="<?= theme_url('assets/css/single.css') ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">

</head>

<body>
    <?php
    include theme_path("view/navbar.php");
    ?>


    <div class='comments'>
        <?php foreach ($cart_items as $item_id => $item) : ?>
            <div class='comment'>
                <div class='comment-info'>
                    <div class='commenter'>
                        <a href="<?= site_url("file?id=$item->id") ?>">
                            <?= $item->title . " ($item->count)" ?>
                        </a>
                    </div>
                    <div class="like">
                        <span class="count">
                            <a href="<?= site_url("cart/remove?id=$item_id") ?>"><span class='fas fa-trash' style="color:red"></span></a>
                        </span>
                    </div>
                </div>
                <div class='comment-content'><?= $item->price ?></div>
            </div>
        <?php endforeach; ?>
    </div>
    <form action="<?= site_url('payment') ?>" method="post">
        <div class='comments pay-type'>
            <label for="pay-w">
                <input type="radio" id="pay-w" name="payType" value='Wallet'> پرداخت کیف پول
            </label>
            &nbsp; &nbsp; &nbsp; &nbsp;
            <label for="pay-o">
                <input type="radio" id="pay-o" name="payType" value='Online' checked> پرداخت آنلاین
            </label>
        </div>
        <div class='cart-buttons'>
            <button type='submit' class='btn btnGreen'>پرداخت نهایی</button>
            <a class='btn btnBlue' href="<?= site_url('') ?>">ادامه خرید</a>
        </div>
    </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- partial -->
    <script src="<?= theme_url('assets/js/single.js') ?>"></script>
    <script src="<?= theme_url('assets/js/common.js') ?>"></script>

</body>

</html>
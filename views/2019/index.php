<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Latest Files ...</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel="stylesheet" href="<?= get_theme_stylesheet() ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
</head>

<body>

    <?php include theme_path("view/navbar.php") ?>

    <?php
    // Scope problem
    // include_theme_path("view/navbar.php") 
    ?>

    <!-- partial:index.partial.html -->
    <header>
        <div class='container'>
            <h1>فایل مارکت سون لرن</h1>
            <p>با کیفیت ترین فایل ها رو از ما بخواهید</p>
        </div>
    </header>
    <div class='container'>
        <ul>
            <?php foreach ($files as $file) : ?>
                <li>

                    <a class='normal' href='<?= site_url("file?id=$file->id") ?>'>
                        <img src='<?= $file->thumb ?? theme_url('assets/img/default-thumb.png') ?>'>
                    </a>
                    <div class='info'>
                        <h3><?= $file->title ?></h3>
                        <p><?= $file->description ?></p>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <!-- partial -->
    <script src="<?= theme_url('assets/js/home.js') ?>"></script>

</body>

</html>
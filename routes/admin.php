<?php
return array(
    /**** Admin Routes *****/
    '/admin' => [
        'method' => 'get',
        'target' => 'Admin\DashboardController@index',
        'middleware' => 'AdminPanelGuard'
    ],

    // File routes
    '/admin/file/add' => [
        'method' => 'get',
        'target' => 'Admin\FileController@add',
        'middleware' => 'AdminPanelGuard'
    ],
    '/admin/file/save' => [
        'method' => 'post',
        'target' => 'Admin\FileController@save',
        'middleware' => 'AdminPanelGuard'
    ],
    '/admin/file/list' => [
        'method' => 'get',
        'target' => 'Admin\FileController@list',
        'middleware' => 'AdminPanelGuard'
    ],
    '/admin/file/delete' => [
        'method' => 'get',
        'target' => 'Admin\FileController@delete',
        'middleware' => 'AdminPanelGuard'
    ],


    // Tag routes
    '/admin/tag/add' => [
        'method' => 'get',
        'target' => 'Admin\TagController@add',
        'middleware' => 'AdminPanelGuard'
    ],
    '/admin/tag/ajaxSave' => [
        'method' => 'post',
        'target' => 'Admin\TagController@ajaxSave',
        'middleware' => 'AdminPanelGuard'
    ],
    '/admin/tag/delete' => [
        'method' => 'get',
        'target' => 'Admin\TagController@delete',
        'middleware' => 'AdminPanelGuard'
    ],
    '/admin/tag/list' => [
        'method' => 'get',
        'target' => 'Admin\TagController@list',
        'middleware' => 'AdminPanelGuard'
    ],
    '/admin/tag/field-edit' => [
        'method' => 'post',
        'target' => 'Admin\TagController@fieldEdit',
        'middleware' => 'AdminPanelGuard'
    ],

    // Tag routes
    '/admin/option/add' => [
        'method' => 'get',
        'target' => 'Admin\OptionController@add',
        'middleware' => 'AdminPanelGuard'
    ],
    '/admin/option/ajaxSave' => [
        'method' => 'post',
        'target' => 'Admin\OptionController@ajaxSave',
        'middleware' => 'AdminPanelGuard'
    ],
    '/admin/option/delete' => [
        'method' => 'get',
        'target' => 'Admin\OptionController@delete',
        'middleware' => 'AdminPanelGuard'
    ],
    '/admin/option/list' => [
        'method' => 'get',
        'target' => 'Admin\OptionController@list',
        'middleware' => 'AdminPanelGuard'
    ],
    '/admin/option/field-edit' => [
        'method' => 'post',
        'target' => 'Admin\OptionController@fieldEdit',
        'middleware' => 'AdminPanelGuard'
    ],


);

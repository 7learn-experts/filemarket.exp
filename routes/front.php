<?php
return array(
    '/' => [
        'method' => 'get',
        'target' => 'HomeController@index'
    ],


    // Front File routes
    '/file' => [
        'method' => 'get',
        'target' => 'FileController@single'
    ],

    // Like Routes
    '/like/add' => [
        'method' => 'post',
        'target' => 'LikeController@add'
    ],

    // Pay Routes
    '/payment' => [
        'method' => 'post',
        'target' => 'PayController@pay'
    ],
    '/payment-verification' => [
        'method' => 'get',
        'target' => 'PayController@verify'
    ],

    // Auth
    '/auth' => [
        'method' => 'get',
        'target' => 'AuthController@index'
    ],
    '/auth/register' => [
        'method' => 'post',
        'target' => 'AuthController@register'
    ],
    '/auth/login' => [
        'method' => 'post',
        'target' => 'AuthController@login'
    ],
    '/auth/logout' => [
        'method' => 'get',
        'target' => 'AuthController@logout'
    ],



    // Cart Routes
    '/cart' => [
        'method' => 'get',
        'target' => 'CartController@index'
    ],
    '/cart/add' => [
        'method' => 'get',
        'target' => 'CartController@add'
    ],
    '/cart/remove' => [
        'method' => 'get',
        'target' => 'CartController@remove'
    ],
);

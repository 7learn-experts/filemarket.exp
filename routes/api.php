<?php
return array(
    '/api/v1/files' => [
        'method' => 'get',
        'target' => 'Api\V1\FileController@list'
    ],
    '/api/v1/file/delete' => [
        'method' => 'delete|post',
        'target' => 'Api\V1\FileController@delete'
    ],
    '/api/v1/file/add' => [
        'method' => 'post',
        'target' => 'Api\V1\FileController@add'
    ],

);

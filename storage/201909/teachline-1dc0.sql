CREATE TABLE `tl_users`
(
  `user_id` bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `user_name` varchar(127),
  `email` varchar(127),
  `password` varchar(63),
  `rol` varchar(127),
  `created_at` datetime
);

ALTER TABLE `tl_users`
    ADD KEY `user_name` (`user_name`),
    ADD KEY `email` (`email`);

CREATE TABLE `tl_teachers`
(
  `teacher_id` bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `user_id` int(11),
  `pachage_id` int(3),
  `last_name` varchar(127),
  `first_name` varchar(127),
  `phone_number` varchar(13),
  `telegram_id` varchar(127),
  `province` varchar(11),
  `city` varchar(11),
  `age` int(3),
  `sex` varchar(11),
  `degree_of_education` varchar(63),
  `skill` varchar(127),
  `description` text,
  `url_thumbnail` longtext
);

ALTER TABLE `tl_teachers`
    ADD KEY `user_id` (`user_id`),
    ADD KEY `pachage_id` (`pachage_id`);

CREATE TABLE `tl_students`
(
  `student_id` bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `user_id` int(11),
  `pachage_id` int(3),
  `last_name` varchar(127),
  `first_name` varchar(127),
  `phone_number` varchar(13),
  `telegram_id` varchar(127),
  `province` varchar(11),
  `city` varchar(11),
  `age` int(3),
  `sex` varchar(11),
  `grade` varchar(63),
  `skills_required` varchar(127),
  `description` text,
  `url_thumbnail` longtext
);

ALTER TABLE `tl_students`
    ADD KEY `user_id` (`user_id`),
    ADD KEY `pachage_id` (`pachage_id`);

CREATE TABLE `tl_packages`
(
  `package_id` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `title` varchar(127),
  `monthly_prise` int(11),
  `settings` text,
  `status` varchar(63)
);

CREATE TABLE `tl_posts`
(
  `post_id` bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20),
  `post_title` text,
  `post_content` longtext,
  `post_excerpt` text,
  `post_date` datetime,
  `post_permalink` text,
  `post_thumbnail` longtext,
  `post_parent` bigint(20),
  `post_status` varchar(20),
  `commnet_status` varchar(20),
  `comment_count` int(11),
  `user_id` bigint(20)
);

ALTER TABLE `tl_posts`
    ADD KEY `post_author` (`post_author`),
    ADD KEY `post_parent` (`post_parent`);

CREATE TABLE `tl_postmeta`
(
  `meta_id` bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20),
  `meta_key` varchar(255),
  `meta_value` longtext
);

ALTER TABLE `tl_postmeta`
    ADD KEY `post_id` (`post_id`);

CREATE TABLE `tl_categories`
(
  `cat_id` bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20),
  `cat_name` varchar(127),
  `cat_slug` text,
  `parent_id` bigint(20)
);

ALTER TABLE `tl_categories`
    ADD KEY `post_id` (`post_id`),
    ADD KEY `parent_id` (`parent_id`);

CREATE TABLE `tl_comments`
(
  `comment_id` bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20),
  `comment_post_id` bigint(20),
  `comment_author` varchar(127),
  `comment_author_email` varchar(127),
  `comment_author_IP` varchar(127),
  `comment_date` datetime,
  `comment_content` text,
  `comment_status` varchar(63),
  `comment_parent` bigint(20),
  `user_id` bigint(20)
);

ALTER TABLE `tl_comments`
    ADD KEY `post_id` (`post_id`),
    ADD KEY `user_id` (`user_id`);

CREATE TABLE `tl_payments`
(
  `payment_id` bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20),
  `package_id` bigint(20),
  `payment_status` varchar(63),
  `payment_gateway` varchar(63),
  `paid_amount` int(11),
  `expiration_date` datetime,
  `payment_data` datetime
);

ALTER TABLE `tl_payments`
    ADD KEY `user_id` (`user_id`),
    ADD KEY `package_id` (`package_id`);

CREATE TABLE `tl_options`
(
  `option_id` bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `option_name` varchar(255),
  `option_value` longtext
);

ALTER TABLE `tl_students` ADD FOREIGN KEY (`user_id`) REFERENCES `tl_users` (`user_id`);

ALTER TABLE `tl_teachers` ADD FOREIGN KEY (`user_id`) REFERENCES `tl_users` (`user_id`);

ALTER TABLE `tl_payments` ADD FOREIGN KEY (`user_id`) REFERENCES `tl_users` (`user_id`);

ALTER TABLE `tl_payments` ADD FOREIGN KEY (`package_id`) REFERENCES `tl_packages` (`package_id`);

ALTER TABLE `tl_posts` ADD FOREIGN KEY (`user_id`) REFERENCES `tl_users` (`user_id`);

ALTER TABLE `tl_postmeta` ADD FOREIGN KEY (`post_id`) REFERENCES `tl_posts` (`post_id`);

ALTER TABLE `tl_comments` ADD FOREIGN KEY (`user_id`) REFERENCES `tl_users` (`user_id`);

ALTER TABLE `tl_categories` ADD FOREIGN KEY (`post_id`) REFERENCES `tl_posts` (`post_id`);

ALTER TABLE `tl_comments` ADD FOREIGN KEY (`post_id`) REFERENCES `tl_users` (`user_id`);


<?php

function config($name)
{
    $config = include BASE_PATH . "configs/$name.php";
    return $config;
}

function my_dd($args)
{
    echo "<pre style='background:#f7f7f7'>";
    var_dump($args);
    echo "</pre>";
    die();
}


function site_url($uri = '')
{
    return BASE_URL . $uri;
}


function storage_url($filename)
{
    return site_url("storage/$filename");
}

function storage_path($filename)
{
    return STORAGE_PATH . $filename;
}


function admin_url($uri)
{
    return site_url('admin/' . $uri);
}
function theme_url($uri)
{
    $active_theme = getOption('active_theme');
    return site_url("views/$active_theme/$uri");
}

function theme_path($uri)
{
    $active_theme = getOption('active_theme');
    return BASE_VIEW_PATH . "$active_theme/$uri";
}

function include_theme_path($uri)
{
    $active_theme = getOption('active_theme');
    include BASE_VIEW_PATH . "$active_theme/$uri";
}

function asset($filepath)
{
    $active_theme = getOption('active_theme');
    return site_url('views/' . $active_theme . '/assets/' . $filepath);
}

function asset_admin($filepath)
{
    return site_url('views/admin/assets/' . $filepath);
}


function removeEmptyMembers($array)
{
    return array_filter($array, function ($a) {
        return trim($a) !== "";
    });
}


function array_of_objects($arr2d)
{
    $array_of_objects = array();
    foreach ($arr2d as $arr) {
        $array_of_objects[] = (object) $arr;
    }
    return $array_of_objects;
}

// wrapper to Option::get()
function getOption($key)
{
    return \App\Utilities\Option::get($key);
}



// theme functions 

function get_theme_stylesheet()
{
    return theme_url('style.css');
}


// castAs($object,File::class);
function castAs($sourceObject, $newClass)
{
    $castedObject                    = new $newClass();
    $reflectedSourceObject           = new \ReflectionClass($sourceObject);
    $reflectedSourceObjectProperties = $reflectedSourceObject->getProperties();

    foreach ($reflectedSourceObjectProperties as $reflectedSourceObjectProperty) {
        $propertyName = $reflectedSourceObjectProperty->getName();

        $reflectedSourceObjectProperty->setAccessible(true);

        $castedObject->$propertyName = $reflectedSourceObjectProperty->getValue($sourceObject);
    }
}
